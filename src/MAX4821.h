//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifndef MAX4821_H_
#define MAX4821_H_

#include <stdbool.h>
#include <stdint.h>

class MAX4821
{
public:
    enum coils_t : uint8_t{
        MAX4821_COIL_1 = 0x00,
        MAX4821_COIL_2 = 0x01,
        MAX4821_COIL_3 = 0x02,
        MAX4821_COIL_4 = 0x03,
        MAX4821_COIL_5 = 0x04,
        MAX4821_COIL_6 = 0x05,
        MAX4821_COIL_7 = 0x06,
        MAX4821_COIL_8 = 0x07,
    };

    MAX4821();
    virtual ~MAX4821();

    virtual bool reset();

    virtual bool set();

    bool setOutput(const uint8_t coil, const bool enabled);

    bool getOutput(const uint8_t coil, bool &enabled);

protected:
    uint8_t outputs_;     //nth bit set indicates nth output is enabled

private:
    virtual bool setAddress(const uint8_t output) = 0;
    virtual bool setLevel(bool enabled) = 0;
    virtual bool setCS(bool enabled) = 0;

    virtual void MAX4821DelayUs(uint32_t us) = 0;
};


#endif /* MAX4821_H_ */