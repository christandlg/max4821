//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#include "MAX4821.h"

MAX4821::MAX4821() : 
outputs_(0)
{
}

MAX4821::~MAX4821()
{

}

bool MAX4821::reset()
{
    bool return_value = true;

    for (uint8_t i = 0; i < 8; i++)
        return_value &= setOutput(i, false);

    outputs_ = 0x00;

    return return_value;
}

bool MAX4821::set()
{
    bool return_value = true;

    for (uint8_t i = 0; i < 8; i++)
        return_value &= setOutput(i, true);

    outputs_ = 0xFF;

    return return_value;
}

bool MAX4821::setOutput(const uint8_t output, const bool enabled)
{
    switch (output)
    {
        case MAX4821_COIL_1:
        case MAX4821_COIL_2:
        case MAX4821_COIL_3:
        case MAX4821_COIL_4:
        case MAX4821_COIL_5:
        case MAX4821_COIL_6:
        case MAX4821_COIL_7:
        case MAX4821_COIL_8:
            break;
        default:
            return false;
    }

    if (!setAddress(output))
        return false;

    //set the bit corresponding to the output
    if (enabled)
        outputs_ |= (1 << output);
    else
        outputs_ &= ~(1 << output);


    MAX4821DelayUs(1);

    if (!setCS(false))
        return false;

    if (!setLevel(enabled))
        return false;

    MAX4821DelayUs(1);

    if (!setCS(true))
        return false;

    return true;
}

bool MAX4821::getOutput(const uint8_t output, bool &enabled)
{
    switch (output)
    {
        case MAX4821_COIL_1:
        case MAX4821_COIL_2:
        case MAX4821_COIL_3:
        case MAX4821_COIL_4:
        case MAX4821_COIL_5:
        case MAX4821_COIL_6:
        case MAX4821_COIL_7:
        case MAX4821_COIL_8:
            break;
        default:
            return false;
    }

    enabled = outputs_ & (1 << output);

    return true;    
}

