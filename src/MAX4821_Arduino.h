//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifdef ARDUINO

#ifndef MAX4821_ARDUINO_H_
#define MAX4821_ARDUINO_H_

#include <Arduino.h>

#include "MAX4821.h"

class MAX4821_Arduino : public MAX4821
{
public:
    MAX4821_Arduino(uint8_t MAX4821_A0, uint8_t MAX4821_A1, uint8_t MAX4821_A2, uint8_t MAX4821_CS, uint8_t MAX4821_VL, uint8_t MAX4821_RESET = 255, uint8_t MAX4821_SET = 255);
    virtual ~MAX4821_Arduino();

    virtual bool reset();

    virtual bool set();

private:
    bool setAddress(const uint8_t output);
    bool setLevel(bool enabled);
    bool setCS(bool enabled);

    void MAX4821DelayUs(uint32_t us);

    uint8_t pin_a0_;
    uint8_t pin_a1_;
    uint8_t pin_a2_;

    uint8_t pin_cs_;
    uint8_t pin_lvl_;

    uint8_t pin_reset_;
    uint8_t pin_set_;
};

#endif /* MAX4821_ARDUINO_H_ */

#endif /* ARDUINO */