//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifdef __MBED__

#ifndef MAX4821_MBED_H_
#define MAX4821_MBED_H_

#include <mbed.h>

#include "MAX4821.h"

class MAX4821_mbed : public MAX4821
{
public:
    MAX4821_mbed(PinName A0, PinName A1, PinName A2, PinName CS, PinName LVL, PinName RESET = NC, PinName SET = NC);
    virtual ~MAX4821_mbed();

    virtual bool reset();

    virtual bool set();

private:
    bool setAddress(const uint8_t output);
    bool setLevel(bool enabled);
    bool setCS(bool enabled);

    void MAX4821DelayUs(uint32_t us);

    DigitalOut *pin_a0_;
    DigitalOut *pin_a1_;
    DigitalOut *pin_a2_;

    DigitalOut *pin_cs_;
    DigitalOut *pin_lvl_;

    DigitalOut *pin_reset_;
    DigitalOut *pin_set_;
};

#endif /* MAX4821_MBED_H_ */

#endif /* __MBED__ */
