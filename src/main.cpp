//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#include <mbed.h>

#include "MAX4821_mbed.h"

BufferedSerial serial(USBTX, USBRX);

MAX4821_mbed max4821(D3, D4, D5, D6, D7, D8, D9);

int main() {

  // put your setup code here, to run once:

  string message = "main.cpp start";
  message += "\n";

  serial.write(message.c_str(), message.length());

  max4821.reset();

  wait_us(5e6);
  


  while(1) {

    for (int i = 0; i < 8; i++)
    {
      if (max4821.setOutput(i, true))
      {
        message = "enabled coil ";
        message += to_string(i);
        message += "\n";
      }
      else
      {
        message = "error enabling coil";
        message += to_string(i);
        message += "\n";
      } 

      serial.write(message.c_str(), message.length());
    }    

    for (int i = 0; i < 8; i++)
    {
      if (max4821.setOutput(i, false))
      {
        message = "disabled coil ";
        message += to_string(i);
        message += "\n";
      }
      else
      {
        message = "error disabling coil";
        message += to_string(i);
        message += "\n";
      } 

      serial.write(message.c_str(), message.length());
    }

    wait_us(1e6);
  }

}