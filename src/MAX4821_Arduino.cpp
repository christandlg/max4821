//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifdef ARDUINO

#include <MAX4821_Arduino.h>

MAX4821_Arduino::MAX4821_Arduino(uint8_t MAX4821_A0, uint8_t MAX4821_A1, uint8_t MAX4821_A2, uint8_t MAX4821_CS, uint8_t MAX4821_LVL, uint8_t MAX4821_RESET, uint8_t MAX4821_SET) :
    MAX4821(), 
    pin_a0_(MAX4821_A0),
    pin_a1_(MAX4821_A1),
    pin_a2_(MAX4821_A2),
    pin_cs_(MAX4821_CS),
    pin_lvl_(MAX4821_LVL),
    pin_reset_(MAX4821_RESET),
    pin_set_(MAX4821_SET)
{
    digitalWrite(pin_reset_, HIGH);
    digitalWrite(pin_set_, HIGH);
    digitalWrite(pin_cs_, HIGH);

    pinMode(pin_a0_, OUTPUT);
    pinMode(pin_a1_, OUTPUT);
    pinMode(pin_a2_, OUTPUT);
    pinMode(pin_cs_, OUTPUT);
    pinMode(pin_lvl_, OUTPUT);
    pinMode(pin_reset_, OUTPUT);
    pinMode(pin_set_, OUTPUT);
}

MAX4821_Arduino::~MAX4821_Arduino()
{

}

bool MAX4821_Arduino::reset()
{
    //call implementation from MAX4821 class if reset pin is not available
    if (pin_reset_ == 255)
        return MAX4821::reset();
    
    digitalWrite(pin_reset_, LOW);
    MAX4821DelayUs(1);
    digitalWrite(pin_reset_, HIGH);

    outputs_ = 0x00;

    return true;;
}

bool MAX4821_Arduino::set()
{
    //call implementation from MAX4821 class if set pin is not available
    if (pin_set_ == 255)
        return MAX4821::set();
    
    digitalWrite(pin_set_, LOW);
    MAX4821DelayUs(1);
    digitalWrite(pin_set_, HIGH);

    outputs_ = 0xFF;

    return true;
}

bool MAX4821_Arduino::setAddress(const uint8_t output)
{
    if (output > 7)
        return false;

    digitalWrite(pin_a0_, output & 0b001 ? HIGH : LOW);
    digitalWrite(pin_a1_, output & 0b010 ? HIGH : LOW);
    digitalWrite(pin_a2_, output & 0b100 ? HIGH : LOW);

    return true;
}

bool MAX4821_Arduino::setLevel(bool enabled)
{
    digitalWrite(pin_lvl_, enabled ? HIGH : LOW);

    return true;
}

bool MAX4821_Arduino::setCS(bool enabled)
{
    digitalWrite(pin_cs_, enabled ? HIGH : LOW);

    return true;
}

void MAX4821_Arduino::MAX4821DelayUs(uint32_t us)
{
    delayMicroseconds(us);
}

#endif /* ARDUINO */