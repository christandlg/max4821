//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifdef __MBED__

#include <MAX4821_mbed.h>

MAX4821_mbed::MAX4821_mbed(PinName A0, PinName A1, PinName A2, PinName CS, PinName LVL, PinName RESET, PinName SET) :
    MAX4821(), 
    pin_a0_(nullptr),
    pin_a1_(nullptr),
    pin_a2_(nullptr),
    pin_cs_(nullptr),
    pin_lvl_(nullptr),
    pin_reset_(nullptr),
    pin_set_(nullptr)
{
    pin_a0_ = new DigitalOut(A0);
    pin_a1_ = new DigitalOut(A1);
    pin_a2_ = new DigitalOut(A2);

    pin_cs_ = new DigitalOut(CS);
    pin_lvl_ = new DigitalOut(LVL);

    if (RESET != NC)
        pin_reset_ = new DigitalOut(RESET);
    
    if (RESET != SET)
        pin_set_ = new DigitalOut(SET);
}

MAX4821_mbed::~MAX4821_mbed()
{
    if (pin_a0_)
    {
        delete pin_a0_;
        pin_a0_ = nullptr;
    }
    if (pin_a1_)
    {
        delete pin_a1_;
        pin_a1_ = nullptr;
    }
    if (pin_a2_)
    {
        delete pin_a2_;
        pin_a2_ = nullptr;
    }
    
    if (pin_cs_)
    {
        delete pin_cs_;
        pin_cs_ = nullptr;
    }
    if (pin_lvl_)
    {
        delete pin_lvl_;
        pin_lvl_ = nullptr;
    }
    
    if (pin_reset_)
    {
        delete pin_reset_;
        pin_reset_ = nullptr;
    }
    if (pin_set_)
    {
        delete pin_set_;
        pin_set_ = nullptr;
    }
}

bool MAX4821_mbed::reset()
{
    //call implementation from MAX4821 class if reset pin is not available
    if (!pin_reset_)
        return MAX4821::reset();
    
    pin_reset_->write(0);
    MAX4821DelayUs(1);
    pin_reset_->write(1);

    outputs_ = 0x00;

    return true;;
}

bool MAX4821_mbed::set()
{
    //call implementation from MAX4821 class if set pin is not available
    if (!pin_set_)
        return MAX4821::set();
    
    pin_set_->write(0);
    MAX4821DelayUs(1);
    pin_set_->write(1);

    outputs_ = 0xFF;

    return true;
}

bool MAX4821_mbed::setAddress(const uint8_t output)
{
    if (output > 7)
        return false;

    pin_a0_->write(output & 0b001 ? 1 : 0);
    pin_a1_->write(output & 0b010 ? 1 : 0);
    pin_a2_->write(output & 0b100 ? 1 : 0);

    return true;
}

bool MAX4821_mbed::setLevel(bool enabled)
{
    pin_lvl_->write(enabled ? 1 : 0);

    return true;
}

bool MAX4821_mbed::setCS(bool enabled)
{
    pin_cs_->write(enabled ? 1 : 0);

    return true;
}

void MAX4821_mbed::MAX4821DelayUs(uint32_t us)
{
    wait_us(us);
}

#endif /* __MBED__ */