# README #

A c++ (mbed) library for the [Maxim MAX4821](https://www.maximintegrated.com/en/products/analog/analog-switches-multiplexers/MAX4820.html) cascadable relay driver.
