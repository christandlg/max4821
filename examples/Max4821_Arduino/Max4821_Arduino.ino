//Maxim MAX4821 relay driver library 
// Copyright (c) 2018-2019 Gregor Christandl <christandlg@yahoo.com>
// home: https://bitbucket.org/christandlg/max4821
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#include <Arduino.h>

#include "MAX4821_Arduino.h"

#define PIN_MAX4821_A0 A0
#define PIN_MAX4821_A1 A1
#define PIN_MAX4821_A2 A2
#define PIN_MAX4821_SET A3
#define PIN_MAX4821_RESET A4
#define PIN_MAX4821_CS A5
#define PIN_MAX4821_LVL A6

MAX4821_Arduino max4821(PIN_MAX4821_A0, PIN_MAX4821_A1, PIN_MAX4821_A2, PIN_MAX4821_CS, PIN_MAX4821_LVL, PIN_MAX4821_RESET, PIN_MAX4821_SET);

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);

  max4821.reset();

  delay(1000); 
}

void loop() {
  // put your main code here, to run repeatedly:

  for (uint8_t i = 0; i < 8; i++)
  {
    Serial.print("toggle coil "); Serial.println(i);
   
    max4821.setOutput(i, true);
    delay(500);
    max4821.setOutput(i, false);
    
    delay(500);
  }
}